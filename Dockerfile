FROM python:3.7.1

RUN pip install --upgrade pip

RUN mkdir /tickets-api
WORKDIR /tickets-api

ENV PYTHONDONTWRITEBYTECODE 1
ENV FLASK_APP "tickets_api"
ENV FLASK_ENV "development"
ENV FLASK_DEBUG True
ENV TICKET_API_CONFIG_FILE /tickets-api/ticket-api.ini
ADD . /tickets-api

RUN pip install -r requirements.txt
RUN pip install .

WORKDIR /tickets-api/tickets_api

EXPOSE 8080
ENTRYPOINT ["uwsgi", "--ini", "uwsgi.ini"]
