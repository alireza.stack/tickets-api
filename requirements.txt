flask==2.0.1
flask-jwt-extended==4.2.3
jsonschema==3.2.0
pytest==6.2.4
coverage==5.5
flask_restful==0.3.9
mongoengine==0.23.1
celery==5.1.2
uwsgi
Redis