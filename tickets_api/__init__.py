from .scripts.load_places_with_layout import load_places_with_layout
from mongoengine import connect as mongoengine_connect
from .errors import TCHandledExceptions, TCBadRequest
from flask import Flask, current_app as app
from .constants import TK_LOGGER_CONFIG
from logging.config import dictConfig
from .api_mixin import TCAPIMixin
import traceback
import json


def create_app(settings_override=None, load_routes=True, with_logger=True):
    """Application factory."""
    if with_logger:
        dictConfig(TK_LOGGER_CONFIG)

    app = Flask(__name__, instance_relative_config=True)

    app.logger.debug("starting Ticket API server...")

    # load configuration
    app.config.from_envvar("TICKET_API_CONFIG_FILE")

    if settings_override:
        app.config.update(settings_override)

    mongo_conf = json.loads(app.config["MONGODB"])
    app.logger.debug("connecting to MongoDB...")
    mongoengine_connect(
        **mongo_conf
    )

    if load_routes:
        from .routes import app_routes
        app_routes(app)

    error_handlers(app)

    add_commands_to_flask(app)

    app.logger.info("Ticket API server started!")
    return app


def add_commands_to_flask(flask_app):
    flask_app.cli.add_command(load_places_with_layout)


def api_errors_handler(e: TCHandledExceptions):
    """
    Catches Exception exceptions and return good error api request.
    """
    api = TCAPIMixin()
    return api.api_error(e.code, msg=str(e))


def generic_errors_handler(e: Exception):
    """
    Catches exceptions and return nicely formatted error response
    """
    api = TCAPIMixin()
    api_code = "server_error"

    app.logger.error(traceback.format_exc())
    return api.api_error(api_code=api_code)


def method_not_allowed(e):
    api = TCAPIMixin()
    return api.api_error("method_not_allowed")


def error_handlers(app: Flask):
    """
    Register error handlers

    :param app: Flask application instance
    """
    app.register_error_handler(TCHandledExceptions, api_errors_handler)
    app.register_error_handler(Exception, generic_errors_handler)
    app.register_error_handler(405, method_not_allowed)
