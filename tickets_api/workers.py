from logging.handlers import RotatingFileHandler
from .celery_app import celery as celery_app
from .constants import TK_LOGGER_CONFIG
from logging import DEBUG, Formatter
from .errors import TCNoMoreUsers
from bson import ObjectId
import logging
import celery


def create_celery_logger_handler(logger, propagate):
    celery_handler = RotatingFileHandler(
        '/tmp/ticket-celery.log',
        maxBytes=1024 * 1024 * 200,  # 200MB
        backupCount=10
    )
    celery_formatter = Formatter(TK_LOGGER_CONFIG['formatters']['default']['format'])
    celery_handler.setFormatter(celery_formatter)

    logger.addHandler(celery_handler)
    logger.logLevel = DEBUG
    logger.propagate = propagate


@celery.signals.after_setup_task_logger.connect
def after_setup_celery_task_logger(logger, **kwargs):
    """ This function sets the 'celery.task' logger handler and formatter """
    create_celery_logger_handler(logger, True)


@celery.signals.after_setup_logger.connect
def after_setup_celery_logger(logger, **kwargs):
    """ This function sets the 'celery' logger handler and formatter """
    create_celery_logger_handler(logger, False)


@celery_app.task
def seating(payload: list):
    from .runcelery import app

    logging.info("in seating...")
    with app.app_context():
        from .models.reservations import Reservation
        from .models.seat_layout import SeatLayout
        from .models.places import Place

        # PLEASE NOTE: as this is a test reservation,
        # all data will be wiped out before storing new reservations
        Reservation.objects.delete()

        # we assume we have only ONE place at the moment
        place = Place.objects.first()
        if not place:
            logging.error("No place is defined to run seating algorithm!")
            return False

        layouts = SeatLayout.objects().order_by("priority", "row", "index").all()
        if not layouts:
            logging.error(f"No layout defined for {place.name}")
            return False

        logging.info(f"Run seating on {payload['user_groups']}")

        group_of_users = []
        for ix, num_of_seats in enumerate(payload['user_groups']):
            num_to_display = ix + 1
            logging.info(f"index: {num_to_display} -> {num_of_seats}")
            for i in range(num_of_seats):
                group_of_users.append(num_to_display)

        flattened_layout = []
        for _layout in layouts:
            flattened_layout.append({
                "id": _layout["id"],
                "priority": _layout["priority"],
                "section": _layout["section"],
                "row": _layout["row"],
                "filling_direction": _layout["filling_direction"],
                "seat_number": _layout["seat"]["number"],
                "seat_index": _layout["seat"]["index"],
                "seat_rank": _layout["seat"]["rank"]
            })

        try:
            spr = None
            seats_to_be_filled = []
            for row in flattened_layout:
                row_detector = f"{row['section']}{row['priority']}{row['row']}"
                row_is_changed = spr != row_detector
                if row_is_changed:
                    if seats_to_be_filled:
                        filling_direction = seats_to_be_filled[0]["filling_direction"]
                        while seats_to_be_filled:
                            if filling_direction == "left":
                                pop_index = 0
                            else:
                                pop_index = len(seats_to_be_filled) - 1

                            seat_to_be_filled = seats_to_be_filled.pop(pop_index)
                            user = group_of_users.pop(0)
                            logging.info(f"user {user} reserved a seat in "
                                         f"section:{seat_to_be_filled['section']} in "
                                         f"row: {seat_to_be_filled['row']} seat #{seat_to_be_filled['seat_number']}"
                                         )
                            reservation = Reservation(
                                seat=ObjectId(seat_to_be_filled['id']),
                                user=user
                            )
                            reservation.save()

                            if not group_of_users:
                                raise TCNoMoreUsers

                    logging.info("now you need to check filling_direction")
                    spr = row_detector
                    logging.info(spr)

                seats_to_be_filled.append({
                    "id": row["id"],
                    "priority": row["priority"],
                    "section": row["section"],
                    "row": row["row"],
                    "filling_direction": row["filling_direction"],
                    "seat_number": row["seat_number"],
                    "seat_index": row["seat_index"],
                    "seat_rank": row["seat_rank"],
                })
        except TCNoMoreUsers:
            logging.info("Users are all seated ;)")