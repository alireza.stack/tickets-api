
from .celery_init import init_celery
from .celery_app import celery
from . import create_app

"""
This script is run via:
    export TICKET_API_CONFIG_FILE=/etc/ticket-api.ini && celery -A ticket_api.runcelery:celery worker -E
"""

app = create_app(load_routes=False, with_logger=False)
init_celery(app, celery)
