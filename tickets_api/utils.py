from mongoengine.fields import DateTimeField
from datetime import datetime
from bson import Decimal128
from bson import ObjectId
from flask import json


class BaseModel(object):
    created_at = DateTimeField(default=datetime.utcnow)
    updated_at = DateTimeField(default=datetime.utcnow)


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        elif isinstance(o, datetime):
            return o.strftime("%Y-%m-%d %H:%M:%S")
        elif isinstance(o, Decimal128):
            return float(o.__str__())

        return json.JSONEncoder.default(self, o)


def get_json(mongo_doc, excludes=None):
    excludes = excludes or []
    if type(mongo_doc) == dict:
        result = json.loads(
            s=JSONEncoder().encode(o=mongo_doc)
        )
    else:
        result = json.loads(
            s=JSONEncoder().encode(
                o=mongo_doc.to_mongo().to_dict()
            )
        )

    for exclude in excludes:
        if exclude in result:
            del result[exclude]

    return result
