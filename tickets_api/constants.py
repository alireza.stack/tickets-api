""" All constants of the application are kept here """

PARKING = "Parking"
AIR_CONDITIONING = "Air conditioning"

# we can add as many sections as we want,
# list of defined sections here make data consistent on our backend DB
SECTION_HALL = "hall"

SECTION_1ST_BALCONY = "1st Balcony"
SECTION_2ND_BALCONY = "2nd Balcony"

LODGE = "Lodge"
LODGE1 = "Lodge 1"
LODGE2 = "Lodge 2"
LODGE3 = "Lodge 3"
LODGE4 = "Lodge 4"
LODGE5 = "Lodge 5"
LODGE6 = "Lodge 6"
LODGE7 = "Lodge 7"
LODGE8 = "Lodge 8"

SECTION_RIGHT_1ST_SIDE_BALCONY = "right 1st side balcony"
SECTION_LEFT_1ST_SIDE_BALCONY = "left 1st side balcony"

SECTION_RIGHT_SIDE_LODGE = "right side lodge"
SECTION_LEFT_SIDE_LODGE = "left side lodge"

SECTION_RIGHT_2ND_SIDE_BALCONY = "right 2nd side balcony"
SECTION_LEFT_2ND_SIDE_BALCONY = "left 2nd side balcony"

LEFT_DIRECTION = "left"
RIGHT_DIRECTION = "right"

TK_LOGGER_CONFIG = {
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'default'
        }
    },
    'root': {
        'level': 'DEBUG',
        'handlers': ['wsgi']
    }
}
