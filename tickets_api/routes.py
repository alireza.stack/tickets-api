from .views.seating import SeatingView
from flask_restful import Api


def app_routes(app):
    api = Api(app)

    #  ROUTES
    api.add_resource(SeatingView, '/seating')
