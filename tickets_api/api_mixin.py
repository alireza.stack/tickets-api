from .errors import TCBadRequest, API_CODES
from flask import current_app as app


class TCAPIMixin(object):
    _dq_filters_allowed = {}

    def api_response(  # pylint: disable=R0201,R0913
        self,
        content=None,
        response_code=200,
        content_type="application/json",
        headers=None,
        error_response=False,
        meta=None
    ):
        """
        Return a response with specified code and content.

        :param content: The response content to return
        :param response_code: HTTP response code
        :param content_type: Response content type.
        :param headers: Additional headers for the response.
        :param error_response: Indicate that content needs to be sent in errors
            key of the response instead of data key.
        :param meta: meta information of response like total count, etc
        :return: response
        """
        content = {} if content is None else content
        _resp = {}
        if content_type == "application/json":
            if error_response:
                _resp["errors"] = content
            else:
                if meta:
                    _resp["meta"] = meta

                _resp["data"] = content

        if headers:
            headers.update({"Content-Type": content_type})
        else:
            headers = {"Content-Type": content_type}

        return _resp, response_code, headers

    def api_error(
        self,
        api_code,  # pylint: disable=R0201
        msg=None,
        headers=None,
    ):
        """
        Error response from the API.

        :param api_code: a numeric api error response code
        :param msg: optional message which if not given is fetched from the
            codes dict.
        :param headers: Any additional headers that should be part of response.
        :return: A valid api response.
        """
        api_resp = API_CODES[api_code]
        resp = {
            "code": api_resp['status_code'],
            "message": msg or api_resp['message']
        }
        app.logger.error(f"returned error: {resp}")

        return self.api_response(
            content=resp,
            response_code=api_resp['status_code'],
            headers=headers,
            error_response=True,
        )

    def _make_response(
        self,  # pylint: disable=R0201
        data=None,
        errors=None
    ):
        """
        Create a jsonapi spec 1.0 compatible response.

        Ref: https://jsonapi.org/format/1.0/
        """
        resp = {}
        if data is not None and errors is not None:
            raise TCBadRequest(
                "Cannot have both data and errors inside the same response."
            )

        if data is not None:
            resp["data"] = data
        elif errors is not None:
            resp["errors"] = errors

        return resp
