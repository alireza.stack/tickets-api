from ..errors import TCBadRequest, TCServerError
from jsonschema import validate, ValidationError
from flask import current_app as app, request
from ..models.reservations import Reservation
from ..schemas import seating_schema
from flask_restful import Resource
from ..api_mixin import TCAPIMixin
from ..workers import seating
from ..utils import get_json
import traceback
import logging


class SeatingView(Resource, TCAPIMixin):
    def post(self):
        try:
            data = request.get_json(force=True)
            validate(data, seating_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise TCBadRequest("invalid_schema")

        seating.delay(data)

        return self.api_response(response_code=200)

    def get(self):
        try:
            reservations_qs = Reservation.objects.all()
            reservations = []
            for reservation in reservations_qs:
                _reservation_doc = get_json(reservation)
                _reservation_doc["seat"] = get_json(reservation.seat)
                reservations.append(_reservation_doc)

            logging.info(f"returning {len(reservations)} reservations")
            return self.api_response(content=reservations)
        except Exception:
            app.logger.error(traceback.format_exc())
            raise TCServerError
