API_CODES = {
    "invalid_request": {
        'status_code': 400,
        'message': 'invalid_request'
    },
    "method_not_allowed": {
        'status_code': 405,
        'message': 'method_not_allowed'
    },
    "server_error": {
        "status_code": 500,
        "message": "server_error"
    }
}


class TCHandledExceptions(Exception):
    """
    Base class of handled exceptions.
    This exceptions instances are turned into good api error when raised.

    Example:

        ```
        raise TCBadRequest("invalid_request")
        ```

        API would return:
        ```
        {
            'errors': {
                'code': 400,
                'message': 'invalid_request'
            }
        }
        ```
    """

    status = None
    code = None


class TCBadRequest(TCHandledExceptions):
    code = "invalid_request"
    status = API_CODES[code]['status_code']


class TCServerError(TCHandledExceptions):
    code = "server_error"
    status = API_CODES[code]['status_code']


class TCMethodNotAllowed(TCHandledExceptions):
    code = "method_not_allowed"
    status = API_CODES[code]['status_code']


class TCNoMoreUsers(TCHandledExceptions):
    code = "no_user_to_seat"
    status = API_CODES["invalid_request"]['status_code']
