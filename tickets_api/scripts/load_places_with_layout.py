from ..models.seat_layout import SeatLayout, SeatItem
from flask.cli import with_appcontext
from flask import current_app as app
from ..models.places import Place
from .sample_data import PLACES
import click


@click.command(name="load_places_with_layout")
@with_appcontext
def load_places_with_layout():
    app.logger.info("loading places into database...")

    for _p in PLACES:
        place = Place.objects(name=_p["name"]).first()
        if place is None:
            place = Place(
                name=_p["name"],
                address=_p["address"],
                amenities=_p["amenities"]
            )
            place.save()
            app.logger.info(f"Place is saved successfully with ID: {place.id}")

        for _lo in _p["layout"]:
            for _seat in _lo["seats"]:
                seat_layout = SeatLayout(
                    place=place.id,
                    section=_lo["section"],
                    priority=_lo["priority"],
                    row=_lo["row"],
                    seat=SeatItem(**_seat),
                    is_front_row=_lo["is_front_row"],
                    filling_direction=_lo["filling_direction"]
                )
                seat_layout.save()
                app.logger.info(f"Section {_lo['section']} seat row {_lo['row']} #{_seat['number']} saved successfully!")
