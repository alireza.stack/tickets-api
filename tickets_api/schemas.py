seating_schema = {
    "title": "Seating Algorithm Payload",
    "description": "Given a list of `groups of users` per rank, basically sizes.",
    "type": "object",
    "properties": {
        "user_groups": {
            "type": "array",
            "items": {
                "type": "integer",
                "minimum": 1,
            },
            "minItems": 1
        }
    },
    "required": ["user_groups"],
    "additionalProperties": False
}
