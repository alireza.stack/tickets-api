from mongoengine import DynamicDocument, ReferenceField, IntField
from .seat_layout import SeatLayout
from ..utils import BaseModel


class Reservation(DynamicDocument, BaseModel):
    meta = {
        "collection": "reservations"
    }

    seat = ReferenceField(SeatLayout, db_field="seat_id")
    # we assume index is user's IDs in order to fetch ticket wallets
    user = IntField(required=True)
