from mongoengine import DynamicDocument, GeoPointField, ListField
from mongoengine.fields import StringField
from ..utils import BaseModel


class Place(DynamicDocument, BaseModel):
    meta = {"collection": "places"}

    name = StringField(required=True, unique=True)  # Pathé Tuschinski, Aan Het Spui,...
    address = StringField(required=True)
    amenities = ListField(field=StringField())
    location = GeoPointField()
