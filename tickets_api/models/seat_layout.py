from mongoengine import DynamicDocument, ReferenceField, EmbeddedDocumentField, DynamicEmbeddedDocument, BooleanField
from ..constants import SECTION_HALL, SECTION_1ST_BALCONY, SECTION_2ND_BALCONY, LEFT_DIRECTION, RIGHT_DIRECTION, LODGE, \
    SECTION_RIGHT_1ST_SIDE_BALCONY, SECTION_LEFT_1ST_SIDE_BALCONY, SECTION_RIGHT_SIDE_LODGE, SECTION_LEFT_SIDE_LODGE, \
    SECTION_RIGHT_2ND_SIDE_BALCONY, SECTION_LEFT_2ND_SIDE_BALCONY, LODGE1, LODGE2, LODGE3, LODGE4, LODGE5, LODGE6, \
    LODGE7, LODGE8
from mongoengine.fields import StringField, IntField
from ..utils import BaseModel
from .places import Place


class SeatItem(DynamicEmbeddedDocument):
    number = IntField(min_value=1, required=True)  # seat number
    rank = IntField(min_value=1, required=True)

    # using index, seats can be placed in sections in the order we need them to be.
    # like in first row seats will be placed as 2,4,5,3,1.
    # Based on above row, seat number 2 for instance will have the value of 1 for index.
    index = IntField(min_value=1, required=True)

    is_aisle = BooleanField(default=False)
    is_high_seat = BooleanField(default=False)
    is_blocked = BooleanField(default=False)


class SeatLayout(DynamicDocument, BaseModel):
    meta = {
        "collection": "theater_layouts",
        "indexes": [
            {
                "fields": ("place", "section", "row", "seat.index"), "unique": True
            }
        ]
    }

    place = ReferenceField(Place, db_field="place_id")
    # based on priority we know beforehand
    # which sections are important and need to be filled first. By sorting based on (priority,row,index) fields
    priority = IntField(min_value=1, required=True)
    section = StringField(choices=[
        SECTION_HALL,
        SECTION_1ST_BALCONY,
        SECTION_2ND_BALCONY,
        LODGE,
        LODGE1,
        LODGE2,
        LODGE3,
        LODGE4,
        LODGE5,
        LODGE6,
        LODGE7,
        LODGE8,
        SECTION_RIGHT_1ST_SIDE_BALCONY,
        SECTION_LEFT_1ST_SIDE_BALCONY,
        SECTION_RIGHT_SIDE_LODGE,
        SECTION_LEFT_SIDE_LODGE,
        SECTION_RIGHT_2ND_SIDE_BALCONY,
        SECTION_LEFT_2ND_SIDE_BALCONY,
    ], required=True)
    row = IntField(min_value=1, required=True)
    seat = EmbeddedDocumentField(SeatItem, default=SeatItem())
    is_front_row = BooleanField(default=False)
    filling_direction = StringField(choices=[LEFT_DIRECTION, RIGHT_DIRECTION], required=True)
